﻿using System.Web;
using System.Web.Mvc;

namespace DAEAvz_lab10
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
